import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { ContactComponent } from './components/contact/contact.component';
import { InterestsComponent } from './components/interests/interests.component';


const routes: Routes = [
  {
    path:'',component:HomeComponent,
  },
  {
    path:'contact',component:ContactComponent
  },
  {
    path:'interests',component:InterestsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
